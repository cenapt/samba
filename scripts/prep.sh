#!/bin/sh

cat > /etc/samba/smb.conf <<EOF
[global]
  workgroup = ${WORKGROUP}
  nt acl support = yes
  ldap ssl = off
  socket options = TCP_NODELAY SO_RCVBUF=8192 SO_SNDBUF=8192 SO_KEEPALIVE
  server string = ${SERVERSTRING}
  passdb backend = ldapsam:ldap://${LDAP_HOST}
  log level = 0 passdb:0 auth:0
  log file = /var/log/samba/log.%m
  max log size = 50
  os level = 65
  wins support = Yes
  ldap admin dn = cn=Manager,${LDAP_DC}
  ldap group suffix = ou=Group
  ldap passwd sync = yes
  ldap suffix = ${LDAP_DC}
  ldap user suffix = ou=People
EOF

cat > /etc/smbldap-tools/smbldap_bind.conf <<EOF
slaveDN="cn=Manager,${LDAP_DC}"
slavePw="${SAMBA_PW}"
masterDN="cn=Manager,${LDAP_DC}"
masterPw="${SAMBA_PW}"
EOF

cat > /etc/smbldap-tools/smbldap.conf <<EOF
slaveLDAP="ldap://${LDAP_HOST}/"
masterLDAP="ldap://${LDAP_HOST}/"
suffix="${LDAP_DC}"
ldapTLS="0"
EOF

smbpasswd -w ${SAMBA_PW}

smbldap-populate

touch /CONFIGURED
