if [[ ! -f /CONFIGURED ]]; then
    /prep.sh || (echo "Configuration failed" ; exit 1)
fi

net getlocalsid && smbd -i || (echo "Failed to check configuration" ; exit 1)
