# ENV needed:
# SLAPD_DC: eg dc=auth,dc=foo,dc=com
# SLAPD_HOST: auth.foo.com
# WORKGROUP: FOO
# SERVERSTRING: Foo samba

# If it is the first time it is run with LDAP
# You have to run
# /prep.sh <managerpassword>
FROM centos:7


ENV LDAP_DC dc=auth,dc=example,dc=com
ENV LDAP_HOST localhost
ENV WORKGROUP example
ENV SERVERSTRING "Example Samba"
ENV SAMBA_PW test

RUN  yum -y --enablerepo=extras install epel-release && yum clean all && yum -y install samba openldap-clients smbldap-tools
COPY scripts/prep.sh /prep.sh
COPY scripts/run.sh /run.sh
CMD /run.sh
